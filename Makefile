install:
	ln -s ${PWD}/aws-mfa ${HOME}/.local/bin/aws-mfa

uninstall:
	rm -rf ${HOME}/.local/bin/aws-mfa

update_version:
	sed -i -E 's/VERSION=".*?"/VERSION="$(version)"/g' aws-mfa
	git commit -am "Release $(version)"
	git tag -a v$(version) -m "Release $(version)"
