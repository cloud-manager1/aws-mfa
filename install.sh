#!/usr/bin/env bash

INSTALL_PREFIX="$HOME/.cloudmanager"
AWSMFA_REPO="https://gitlab.com/cloud-manager1/aws-mfa.git"

abort() {
    printf "%s\n" "$1"
    exit 1
}

if [[ "$UID" == "0" ]]; then
    abort "Don't run this script as root!"
elif [[ -d "$INSTALL_PREFIX" && ! -x "$INSTALL_PREFIX" ]]; then
    abort "$(
        cat <<EOF
The install prefix for aws-mfa, ${INSTALL_PREFIX}, exists but is not searchable. If this is
not intentional, please restore the default permissions and try running the installer again:
    sudo chmod 775 ${INSTALL_PREFIX}
EOF
    )"
fi

_show_spinner() {
    local -r pid="$!"
    local -r delay='0.5'
    local spinstr='\|/-'
    local temp
    while ps a | awk '{print $1}' | grep -q "${pid}"; do
        temp="${spinstr#?}"
        printf " [%c]  " "${spinstr}"
        spinstr=${temp}${spinstr%"${temp}"}
        sleep "${delay}"
        printf "\b\b\b\b\b\b"
    done
    printf "    \b\b\b\b"
}

_print_profile_setup_help() {
    case "$SHELL" in
    */bash*)
        shell_profile="$HOME/.bashrc"
        ;;
    */zsh*)
        shell_profile="$HOME/.zshrc"
        ;;
    *)
        shell_profile="$HOME/.profile"
        ;;
    esac

    cat <<EOF

NOTE: Configure the AWS MFA toolkit in your $shell_profile by running:

    echo 'export PATH=$INSTALL_PREFIX/bin:\$PATH' >> $shell_profile

EOF
}

_install() {
    printf "This script will install:\n"
    printf "    %s/bin/aws-mfa\n" "$INSTALL_PREFIX"

    printf "\nDownloading and installing AWS MFA toolkit...\n"

    pushd $(mktemp -dq) >/dev/null

    (git clone $AWSMFA_REPO -q) &
    _show_spinner

    mkdir -p "$INSTALL_PREFIX/bin"

    cp aws-mfa/aws-mfa "$INSTALL_PREFIX/bin/aws-mfa"
    chmod +x "$INSTALL_PREFIX/bin/aws-mfa"

    printf "\nAWS MFA toolkit successfully installed!\n\a"

    if [[ ":$PATH:" != *":$INSTALL_PREFIX/bin:"* ]]; then
        _print_profile_setup_help
    fi
}

_install
